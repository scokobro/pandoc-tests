---
title: Pandoc Processor
author: SPKB
---

# Build documents from Markdown source #

This project uses a CI/CD pipeline using Pandoc to build multiple documents (Microsoft Word-docx, OpenOffice-odt, pdf, html) from a markdown source.

The results can be seen [here](./index.html)

Simples.

---
