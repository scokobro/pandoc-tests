---
author: First Second
title: "Paper Title"
bibliography: minibib.bib
csl: apa6.csl
---



# Top-level header #

First paragraph after header. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus @KB:2012[22].

1. list one
2. list two
3. list three

Donec sed odio dui. See @tbl:ref below. And does this filter work - +sb, +hbt. Cum sociis natoque penatibus et magnis dis parturient montes, [nascetur ridiculus mus]{custom-style="spkb1"}.

header one | header two
---|---
content one  |  content two

: captions content {#tbl:ref}

Konno's work *I am the Walrus* -@Konno:2004 says cras justo odio, dapibus ac facilisis in, egestas eget quam. Nullam id dolor id nibh ultricies vehicula ut id elit. And goes on;

>Sed posuere consectetur est at lobortis. Nulla vitae elit libero, a pharetra augue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas faucibus mollis interdum. Sed posuere consectetur est at lobortis.

## Second-level header ##

Cum sociis *natoque penatibus et magnis* dis parturient montes, nascetur ridiculus mus[^fn]. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. **Vestibulum** id ligula porta felis euismod semper. See @fig:ref for details.

[^fn]: footnote content

Cras justo odio, dapibus ac facilisis in, egestas eget quam. Cras mattis consectetur purus sit amet fermentum.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. More importantly here is some code:

```
very important indeed!
```

### Third-level header ###

Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.

* Item first
* Item second
* Item third

Maecenas sed diam eget risus varius blandit sit amet non magna. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.

Term

:  Definition goes here I think

Cras justo odio, dapibus ac facilisis in, egestas eget quam. Nullam id dolor id nibh ultricies vehicula ut id elit. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit. Maecenas faucibus mollis interdum. Maecenas faucibus mollis interdum. Donec id elit non mi porta gravida at eget metus.

# References #
