---
author: "Scott Koga-Browes"
title: "Kyoto's Public Radio Towers"
date: ""
bibliography: mybib.bib
csl: apa6.csl
output:
  custom_document:
    path: /exports/radiotowers.odt
    pandoc_args: [
      "--filter", "pandoc-crossref"]
figureTitle: "Figure"
tableTitle: "Table"
figPrefix: "figure"
tblPrefix: "table"
loftitle: "# List of figures"
lotTitle: "# List of tables"
pdf-engine: "xelatex"
documentclass: "scrartcl"
classoption: "headings=normal"
fontsize: "11pt"
secnumdepth: "2"
pandocomatic_:
    preprocessors: ['/Users/spkb/.local/bin/pp -md -DpaE']
    pandoc:
        output: /Users/spkb/Desktop/RadioTowers/exports/matic.odt
        bibliography: /Users/spkb/Documents/Bibliographies/mybib.bib
        csl: /Users/spkb/Documents/bibliographies/apa6.csl
        from: markdown+raw_attribute+backtick_code_blocks+pipe_tables+yaml_metadata_block
        to: odt
        filter: pandoc-crossref
        number-sections: true
---

<!--
 !exec(echo '' > !root/images-at-end.md)
!def (p)(!ifdef(pae)(
**IMAGE !add(ct)!ct HERE**

!exec(echo '![!1](!2){ !3 }' >> !root/images-at-end.md; echo '' >> !root/images-at-end.md))
(![!1](!2){ !3 }))
-->
File generated: !exec(date +'%H:%M %d/%m/%Y')


To use - [@ichima17] [@iida08] [@robbin01] [@yamagu03a] [@takaha13] [@matsum08]

# Introduction #

The successful establishment of a mixed public-private television broadcasting system in Japan in the years after World War Two can in part be traced to the promotional abilities of Shōriki Matsutaro, the leader of Japan's first commercial broadcaster, Nippon TV. In order to introduce the new technology to the mass audience he understood to be a prerequisite for successful advertising-driven broadcasting, he set up 'street-corner'[^sc] televisions across Japan. These were placed in public squares near busy railway stations and other places where crowds could gather [@yoshim05]. His plan seems visionary until one takes into account the fact that many Japanese at this time would already have had experience of street-corner broadcasting; this paper covers the establishment and growth of the forerunner of Shōriki's public televisions, the extensive, yet now largely forgotten, system of street-corner radios, which were established throughout Japan during the period 1930 -- 1945 by the Japan Broadcasting Corporation (_Nihon Hōsō Kyōkai_, hereon NHK). A disproportionate number of still extant radio-towers are located in the Kansai region, this paper focusses on Kyoto City which is home to about a fifth of Japan's remaining public radios.

[^sc]: The term 'street-corner' is used rather than 'public' in order to avoid confusion with 'public broadcasting' in the sense that NHK is a 'public' rather than 'private' broadcaster.

# Birth of Radio #

The foundation and growth of NHK as Japan's national broadcaster has been covered in detail elsewhere before [eg. @BiJ] [add REFS]{.adm} so this section briefly outlines the major events which concern us here. Broadcasting in Japan started in 1925 with the establishment of three radio stations the three largest cities, Tokyo, Osaka and Nagoya. These separate companies were formed by aggregating the applications of the 64 different organisations and individuals who had applied for permission to broadcast, they included manufacturers of electronics, newspapers and local business leaders. In 1926 these three companies were merged into one, christened NHK, though they retained a degree of local independence in terms of programming until 1934 when changes were made to the managerial structure in order to strengthen central government control [@hiramo10 23--5, 101--2] . From its inception broadcasting in Japan rejected the commercial model established by the earliest stations in the US, NHK was intimately, and 'naturally' it was thought, linked to the state and funded, not through the sales of on-air advertising but, like the BBC in the UK, through the sale of licences to owners of receiving equipment. The dominant thinking being that those who benefitted from radio broadcasting --- primarily listeners and the manufacturers of receivers --- should pay for it. Based on the projected number of subscribers the cost of a monthly license was set initially at two yen in Tokyo and Nagoya, and 1.5 yen in Osaka. However as the number of applications for licenses far exceeded predictions, license fees rapidly fell, to one yen in Jan 1926, then to 75 sen[^1] in April 1932. By 1935 the cost of a licence had fallen to just a quarter of its original price and cost 50 sen.[@nhknenkan35 255].

First national broadcast - April 1930 (Kyoto Nenpyo - G-photo)

<!--
Radio spread quickly and by 1934 the number of NHK license-holders was up to 1.9 million, though over 80 per cent of licensees lived in one of the three main cities

Radio broadcasting was administered under the same law that governed the point-to-point communication of radio-telegraphy, the prime use of radio until the early 1920s. -->

People wishing to listen (legally) to NHK's broadcasts were required to enter into a receiving contract with NHK. The number of contract-holders provides some indication of the growth of radio-listening in Japan. Contract numbers grew fairly slowly over the first few years of broadcasting (see @tbl:subscriptions). Experimental uses of radio had been going on in Japan since the early 1920s and, in the year of NHK's launch, in addition to licensed listeners there may have been up to 30,000 unlicensed radio hobbyists in the Tokyo area alone [@hiramo10 22].

NHK also grew as an organisation, from 395 employees in 1926 to 2249 in 1932, the number of branches across the country increased from the original three to 19. In 1928 there were enough stations across Japan for a nationwide network to created thus allowing the possibility for the simultaneous broadcast of programming across the whole of the main four islands. One problem however remained, this was the relatively low level of penetration of radio into rural areas which in 1932 had reached only 4.5 per cent, the overall urban penetration level at this time was over 25 per cent [@Kasza:1988 88].

The story of the spread of radios is complex in that it was taking place at a time when many fundamental aspects of daily life were being affected by technological change; Radio --- as a technology --- was novel in that it required the owner of a radio set to rely on things outside their control, the provision of programming from a broadcaster, and the provision of an electrical supply. Merely owning a radio receiver was largely meaningless in the absence of these other developments. However it was only during the second half of the 1920s, as broadcasting was becoming established, that the regular supplies of electricity necessary to run a receiver were available to a significant part of Japan's urban inhabitants.

Let us look briefly at the experience of radio listeners in Kansai's largest city and commercial centre, Osaka. The city government took control of electricity supply in 1923 and in 1929 introduced a special contract aimed at households wishing to listen to the radio during the day. At this time the supply of electricity was limited and households and businesses had to share what was available, with businesses being prioritised during daylight hours. In 1929 about 25,000 of households, of the total of 841,000 (about 5 per cent), had access to a supply which would allow daytime listening; this grew to about 30 per cent (apx. 150,000 households) by 1933 but even so @yamagu03a[146-7] suggests that radio would primarily have been an evening activity, the relevance here being that some of the most popular broadcasts in this early period, and certainly the ones that drew audiences to the radios placed in shops and businesses, were live baseball matches, and these happened during daylight. When live baseball broadcasts began in 1927, 90 per cent of Osaka residents did not have a daytime electricity supply, never mind a radio set.


<!--
The ability to access broadcasts did not simply depend on the ownership of a radio; a ready supply of electricity was also necessary for anything other than a 'crystal set', which only allowed 'personal' listening. Looking at the early years of radio in Osaka; t
(a region often at the forefront of developments in radio --- see below), and Osaka, the region's commercial centre and largest city, developed as follows. -->

<!-- Osaka City government took over the supply of electricity from a number of private suppliers in 1923 and in 1929 introduced a special contract to supply the necessary electricity for radio listening. Electricity supply at this time prioritised industrial users and in 1927, when broadcasts of live baseball matches started, 90 per cent of households did not have a daytime electricity supply [@yamagu03a 146--7]. -->

Encouraging take-up of this new technology beyond the technology hobbyists who provided the bedrock of knowledge and energy for the spread of radio was not entirely straightforward. As can be seen from the figures in @tbl:licences, NHK initially had a problem attracting and retaining listeners, the primary reason being the cost of running a radio receiver; the regular replacement of vacuum tubes was a significant cost. @hiramo10[CHECK!] The 1931 NHK Yearbook (p555) puts the monthly costs at somewhere between 0.2 yen for a crystal set and 3.9 yen for a five-tube set, this is apart from the variety of costs associated with maintaining an electricity supply for the set. These were significantly more than the annual cost of a receiving contract.


| year | year end TOTAL | join | leave | net increase |
| ---- | ----------: | ----: | -----: | ------------: |
| 1925 | 259            | 265  | 12    | 253          |
| 1926 | 361            | 184  | 81    | 103          |
| 1927 | 390            | 130  | 101   | 29           |
| 1928 | 565            | 307  | 131   | 176          |
| 1929 | 650            | 253  | 167   | 86           |
| 1930 | 779            | 301  | 173   | 128          |
| 1931 | 1056           | 465  | 188   | 277          |
: New contracts and withdrawals 1925-31, thousands of contracts [@hiramo10 245] {#tbl:licences}


<!-- 1N1Iks1APrSKqvWaOnUiScfHvlgeM0o96ahFMp4IHquo -->

![Changes in NHK contract numbers, 1925-31, Source @hiramo10[245].](./NHK new contracts 1925.svg){ width=90% #fig:NHK-contracts}

However, those who did not yet own a radio set of their own may well have had previous experience of radio-listening --- at that time a new type of activity --- as during this early period radios were often purchased by cafes, restaurants or other shops to attract customers.

Over the first few years of its existence NHK came to perceive itself as having a public function which extended beyond its subscribers to the Japanese public in general. The *1932 Radio Yearbook* (pp 102-8) includes a section on the public service aspects of NHK's work where it outlines public listening sessions over the past year (aimed at encouraging radio listening), and for the first time, mentions 'radio towers', or as it refers to them 'Public-use radio listening facilities' (_Kōshū-yō rajio chōshu shisetsu_).

Throughout the 1930s NHK undertook an expanding series of efforts to encourage radio listening; offices were set up across the country to offer advice on radio equipment and reception, donations of radio receivers were made to schools and various community groups, thousands of people attended open lectures aimed at improving the knowledge and skills of radio technicians. However, this undertaking was far from straightforward;

> Due to the profusion of small rural communities, the project to bring radio into the everyday life of every Japanese was not entirely successful, but by 1941 the results were impressive [@Kasza:1988 252-3]

During 1932, plans were made for the establishment of 50 radio towers, these were seen as part of the marking of NHK's millionth listener licence which took place some time in 1931. Regional branches were asked to propose sites which were then reviewed by the Tokyo office. Tokyo also oversaw the design of towers asking experts to propose designs and eventually selecting 10 alternative designs. Of the originally proposed 50 sites, several turned out to be unusable for various reasons (supply of electricity, concerns of landowner etc) and 36 new towers were eventually built. This was in addition to the four towers already in place around the Kansai region, in Osaka (Tennoji), Kyoto (Maruyama Park), Kobe (Minatogawa Park) and Nara (Sarusawa-no-ike Park) [@nhknenkan33 660].


[^1]: 1 yen = 100 sen

Enthusiasm for radio contributed to the prosperity of other branches of the media; in the mid-1920s some of the larger city-based papers began to offer supplements, often printed on coloured paper, focussing on radio. These typically covered the day's scheduled programs, interviews with performers and the lyrics to songs broadcast. The _Yomiuri Shimbun_, led by Shōriki Matsutaro and with a readership in the region of 50,000, was said to have increased its sales by 'thousand per month' in the period after its decision to start including a the 'Yomiuri Rajio-ban' insert  @nhk01-1[36]. For the Yomiuri this new opportunity appeared just in time to see them through the difficult period after the Great Kanto Earthquake of September 1923 had destroyed their just-completed Ginza main office.

## The Showa Accession and 'Rajio Taisō'

Another element of the story of public radio in Japan is the appearance of 'rajio taisō' --- morning group calisthenics led by a radio broadcast --- which appeared during the same period.

'Radio exercises' had appeared in the US in the early 1920s and the idea was introduced into Japan by Inokuma Teiji, a bureaucrat at the Ministry of Posts (Teishin-sho). During 1928 a proposal was made to the Ministry of Education jointly by Inokuma, the Japan Association of Life insurers and NHK, to begin a radio exercise program in Japan. This was proposed as part of the celebration of the Showa Emperor's accession in 1926, several of the events for which which took place at the Imperial Palace in Kyoto in the period 10-15 November 1928. Nationwide broadcasts of the 'Peoples' Health Exercises' (_kokumin hoken taiso_) program started at 7am on 1 November 1928. Again however, the Kansai region seems to have been in the lead here too; NHK's Osaka station had tried a similar one-off program during the month of August earlier in the year, this was run in cooperation with the local board of education and had consisted of unaccompanied callisthenics instructions, the new national radio exercises, on the other hand, had their own specially created music and movements.[@kuroda99]

Group, sometimes 'mass', callisthenics had proved a focus for national awareness in other countries too; the _Sokol_ (Falcon) movement centred on Czechoslovakia acted as a focus for Czech ethnic feeling and a physical manifestation of the desire for independence from Austro-Hungary. In passing it might also be noted that some of the exercises practised by Sokol groups are highly reminiscent of the 'kumi-taisō'[^kumi] still performed during many Japanese school sports days. @kuroda99 mentions that administrators involved in the setting up of Japan's public exercise program, having travelled in the USA and Europe, were aware of movements such as Sokol.

[^kumi]: Group exercises where several participants stand/kneel/sit on each others knees/shoulders/backs to form elaborate display shapes.

<!-- [Wiki](https://en.wikipedia.org/wiki/Sokol)  -->

As Japan headed further into the mire of its various Asian and Pacific wars, radio calisthenics took on, at least as far as the state was concerned, a deeper significance;

> A core component of spiritual mobilization [Japan's "secret weapon"] was physical fitness, and people were encouraged to exercise to the beat of Radio Calisthenics (Rajio taisō) broadcasts.
> [...]
> The radio was new, modern, and exciting in the 1930s, playing a vital role in disseminating information and uniting the population in common purpose and action. Like boot camp, the goal of Radio Calisthenics was conditioning the body while disciplining the mind and subjugating the individual to authority. [...] Radio Calisthenics became a tool of mass mobilization, mass organization, and mass psychology. [@earhar15 112]

Radio listening, participation as a member of the national audience, had become a patriotic duty, and NHK's effort to make radio available to the widest possible audience, enabled and encouraged this participation.

# The State and Radio #

After its initial brief period as a group of three relatively independent city-based broadcasters, the merger of these stations saw NHK established as Japan's sole radio broadcaster in 1926. Very rapidly it became understood that radio would be a vital tool in the Japanese state's relationship with citizens, and that government control would be proactive, later moving into mobilisation, and 'extensive' @Kasza:1988[88]. As early as 1931, as the Mukden Incident set Japan on the path to war with China, Japan's government put in place administrative structures that would ensure their message, a *unified* message, would get through to as many readers and listeners as could be reached through newspapers and radio. A new government agency, the _Jōhō Iinkai_ (Information Committee), was formed in September 1931 to take charge of this task [@takeya13 12]. Their task was somewhat simplified by NHK's reliance on newspapers --- already censored --- for its news [@Kasza:1988 94]. However, radio penetration rates during this period were between still only around 25 per cent even for urban areas.

The Japanese state was conscious of the role of new technology in its efforts to create an international image of itself as a state worthy of a respected position on the world stage. @iida08[70] sees the attention paid to the deliberate encouragement of radio, and somewhat later, to demonstrations of experimental television, as a symptom of the mounting nationalism in the international community in the 1930s and Japan's desire to demonstrate its maturity as a modern, that is technologically competent, country. Indeed, the Japanese state's media management policy drew heavily on the latest models from Europe and in particular reproduced the centralising tendencies seen in Germany from the early 1930s. On 18 August 1933 Joseph Goebbels, Hitler's Propaganda Minister, presented the Volksempfänger (People’s Receiver) at an international radio exhibition in Berlin, part of an effort to promote radio listening [@adena13 13]. In February 1934 Japan saw the arrival of its own 'People's Receiver' (*kokumin jushinki*) in the form of the Matsushita Electrics models K1 and K2 [@hiramo10 108].

State events, now --- since the inception of a nationwide radio network in 1928 --- _national_ events, repeatedly provided reasons to encourage the spread of radio

1. the enthronement of Emperor Hirohito in 1925
2. the birth of Crown Prince Akihito in 1933
3. commemoration of 2600th anniversary of accession of Japan's first (mythical) Emperor Jimmu, 1940.

Another significant state-related 'media event' was the result of a decision to celebrate the 'year 2600' in the Gregorian calendar year 1940[ce]{.sc}; this was based on a dating system which puts the origin of the founding of Japan at the start of the (mythical) Emperor Jimmu in 660[bce]{.sc}. This calendar had been in limited use for certain types of official document since the 1870s and 1945, alongside the traditional 'nengo' system of counting regnal years. Thus, the year 1940 was both Showa 15 and *kigen* 2600.

An article entitled 'Broadcasting and the State' penned by Miyamoto Yoshio, a senior bureaucrat in the Ministry of Communications, elucidates contemporary thinking on the relationship of broadcasting --- at that point NHK --- government and the Japanese people; since its inception in 1926 broadcasting in Japan, he explains, has undertaken as its mission the improvement Japanese culture and the building of national spirit (*kokumin-seishin no sakkō*), and it has undertaken this role as a representative of the state (*kokka no daikō-kikan*) [@miyamo39 12]. In the article he also laments that Japan was perhaps late to realise the power of broadcasting and stresses the importance of speedily realising the goal of 'one household, one receiver' (*ikko ichi jushinki*)

# Radio Towers #
Public benefits of 'rajio taiso' - but also certainly an eye on the use of radio as a means of public mobilisation. War with China had been going on since 1931.

Until centralisation in 1934, NHK's Osaka branch showed a greater tendency to innovation than either Tokyo or Nagoya; the first public radio was established at Tennoji in Osaka in August 1930 - perhaps with the specific aim of making available live commentary from the Koshien high-school baseball championship. JOBK, the NHK Osaka station took the lead in establishing radio towers and 1931 saw two more established in Nara, in Sarusawa-no-ike Park, and at Kobe in Minatogawa Park.[NHK1938, p240]
<!-- ∆UKRMAR -->

The Kansai region seems to have been remarkably enthusiastic in its adoption of radio; between figures taken soon after the founding of NHK in September 1925 (Taisho 15/9) and those taken at the point in 1932 (Showa 7/3) when the total number of licenses issued nationally passed the one million mark, the rate of increase in the number of listeners in Kansai was close to double to rate of increase in both Tokyo (Kanto) and Nagoya (Tokai) areas. [RE-CHECK! Nagoya figs]{.adm}

|              | Taisho 15/9 | Showa 7/3 | Rate |
|:-------------|------------:|---------:|-----:|
| Tokyo-Kanto  | 225.05      | 458.68   | 2.04 |
| Osaka-Kansai | 72.33       | 331.46   | **4.58** |
| Nagoya-Tokai | 41.79       | 116.99   | 2.8  |
: Number (thousands) of radio licences issued by main three NHK stations [@nhknenkan33 606]. {#tbl:subscriptions}

<!-- 1YLbfOtHqtlBgqGIydAzCCH2ORnbwar4vLLFt_13vK5g -->

During the following year? the number of NHK subscribers reached one million, and as part of the celebrations marking this milestone, it was decided that 'radio towers', like those set up around Kansai, should be established throughout the rest of Japan. 1932 saw a further 22 towers set up, from Kumamoto in the south of Kyushu to Asahikawa in Hokkaido.

The NHK Kyoto station, JOOK started life in July 1929 as a branch office of the main Osaka station. At first it merely provided information on radio reception for Kyoto listeners but in November 1928 a recording and performance studio recording was added and, after the coverage of the various events connected with the accession of the new Showa Emperor, Kyoto NHK began to produced programming. Kyoto got its first radio tower in 1932 at Maruyama Park (see below).


The 1941 Radio Yearbook(p323) lists five radio towers in Kyoto City; Maruyama Park, Funaoka Park, Tachibana-jido Park, Kosaka Park and Nijo Park[^nijo]. The first three of these are still extant, the latter two still exist as public parks but there is no longer any trace of their radio towers.[CHECK]{.adm} A year later they had been joined by the still extant installation at Yase and another now-missing tower at Nishi Honganji Jido Park ([Yearbook S17](https://www.evernote.com/l/AALj2X2wqxpCM48zeBpdKf7J2RPjQJDIe2o))

[^nijo]: NHK Kyoto's office was located next to this park until its 2015 move to their current premises at Karasuma-Oike. March 1940 - no radio tower mentioned in @koen40[11].

* 1925-07-16  Radio advice office set up at Kawaramachi-Shijo to help listeners to the JOBK Osaka station
* 1928-11-05  Studio set up in department store (Bussankan) near Kyoto Station. Provides programming connected to accession of Show Emperor
* 1932-05-25 Kyoto Broadcast Station licensed as JOOK
* 1932-06-24  Kyoto NHK Offices consolidated at new site just north of Nijo Park.

The use of radio towers spread throughout the territory of Imperial Japan, including to Taiwan - extant example in 臺北市新公園內的臺灣廣播電臺放送亭.embellished with JFAK call sign and shown on p

By 1941, there were 346 towers throughout Japan @nhk01-1[78]

# Currently Extant Towers #

@ichima17 lists 37 surviving pre-war radio towers in Japan and three still standing in Taiwan.

In general plan the towers tend to be 2.5 - 3.5 m tall, though some were close to 5 m tall, and have a round or rectangular cross section 46-60 cm across. Designs vary a great deal, the aim was to allow each individual installation to blend into its surroundings appropriately. Indeed some of the 'towers' are designed into a wider environment (see @fig:murasaki).

Small local parks like the ones where many of the radio towers are placed are typically now managed jointly by a local government office, usually at city or ward level, and a group of local residents who form a Protection Support Association (PSA) (_aigo-kyōryoku-kai_) associated with a particular park. In Kyoto, the local city government's two parks management offices are responsible for the upkeep of the city's 780 green spaces and park facilities - managing vegetation, cleaning, and managing events that use the park space[^2] -  while the PSA takes on itself more of the responsibility for features, such as the radio towers which, since 1945, generally have little more than a decorative(?) function. NHK no longer plays any part in maintaining the radio towers it set up. All management, preservation, restoration initiatives connected to radio towers come exclusively from local, if not hyper-local, voluntary groups.

[^2]: <http://www.city.kyoto.lg.jp/kensetu/page/0000082762.html>

Kyoto, unlike Japan's other cities, was never the target of an allied bombing campaign, it thus preserves much physical pre-war culture that is largely lost in other areas. This includes eight of the 17 radio towers that are still extant in the Kansai area.

![Position of extant Kyoto radio towers](https://i.imgur.com/RVUFrJg.png){ width=90% #fig:map}

These eight towers, primarily in the north of the city, are located as follows;

Location              | City Ward   | Erected | Height(m)
----------------------|-------------|:-------:|---------:
Maruyama Park         | Higashiyama |  1932   |       3.3
Funaoka Park          | Kita        |  1935   |       3.0
Tachibana Park        | Kamigyo     |  1939   |       2.3
Yase Park             | Ukyo        |  1939   |       2.8
Komatsubara Park      | Kita        |  1940   |       2.7
Hagi-jidō Park        | Sakyo       |  1941   |       2.9
Murasakinoyanagi Park | Kita        |  1941   |       2.4
Misayama Park         | Nakagyo     | unknown |       2.7
: Basic data of extant radio towers around Kyoto City {#tbl:basic}

<!-- 1z9gEQDFcbdB67PieHpx9OnkLnQwTIy5fZ4iZPOOleog -->

<!-- [Kyoto Kita-ku Park data](http://www.city.kyoto.lg.jp/kensetu/cmsfiles/contents/0000214/214383/kita.pdf) -->

Their location can perhaps be seen as reflecting Takahashi's -@takaha13[152--4] observation that public radios --- and the participants in radio exercises they were aimed at encouraging --- were primarily located in less affluent areas, where people were less likely to either own a radio or have the private space in which to perform morning exercises.

They may also reflect the extent of electrification inside Kyoto City; a 1932 map in the records of Kyoto City Electric shows supply largely confined to the more northern areas of the city, largely north of Shijo (roughly the area shown in @fig:map). The following brief sections outline the significant features and placement of the remaining radio towers.

## Maruyama Park ## {.unnumbered}
This was the first radio tower erected in Kyoto, and one of the first in the whole of Japan. It is positioned in the centre of a wide expanse of grass making it possible for listeners to gather around it, and engage in morning exercises.

Described in the 1936 Yearbook (p356):

<!-- > 高１１．５尺、鉄筋混凝土 上部花崗岩、角燈籠型 (p356) -->
> Height 11.5 shaku _[3.5 m]_ reinforced concrete, upper portion granite, square-lantern shaped

![Radio tower in Maruyama Park, Kyoto.](https://i.imgur.com/Sb4l8d3.png){width=60% #fig:m-yama}

This radio tower is approximately 1200 m from the NHK Kyoto office which, at the time of its erection, was situated at the junction of Shijo and Kawaramachi streets, still the city's commercial centre.[^nhk-shijo]

[^nhk-shijo]: https://www.nhk.or.jp/kyoto/station_info/history.html

Of the radio towers extant in Kyoto this is probably the grandest, being significantly larger than others; it is approximately 3.3 m tall (4.2 m including the direction marker on top) its sides are roughly 1.3m across. This is in keeping with its position in one of Kyoto's most important public parks, attached to the Gion Shrine complex, Kyoto's 'spiritual guardian'. Maruyama Park is also the oldest of Kyoto's public parks, being designated as such on 25 December 1886 and having reached its current extents by 1912. [City site](http://www.city.kyoto.lg.jp/kensetu/page/0000083282.html)

This tower was refurbished in 1982 when most of its bronze fittings seem to have been replaced. What was once the button used to activate the radio and speakers can be seen in the  centre of the face under the plaque detailing the tower's history and restoration (see @fig:m-yama).

The origin of the radio tower phenomenon is linked to the national enthusiasm for baseball and the fervour with which fans followed the regular (twice yearly?) Kōshien Middle and High-school Baseball tournament held in Osaka. While records related to towers are fairly rare, Kyoto City archives record applications from NHK Kyoto applying for permission to set up a 2.7m x 1.0m blackboard next to the northern side of the radio tower to record the scores of the baseball matches broadcast live from the Kōshien tournaments.[cite records?]{.adm}

## Funaoka-yama Park ## {.unnumbered}

The current park, after a long history as a place where locals could enjoy the landscape and scenic views offered by the 100 m elevation of the central hill, was opened as a public park in Nov 1935 after new city regulations raised funds to arrange a 50-year lease of land owned by the nearby Daitoku-ji temple. The radio tower was installed in 1935 during the improvements to the park undertaken at a cost of JPY288,000 in the period 1932--5 [@koen40 4]. It is briefly described in the 1936 Radio Yearbook (p356) as follows;

> _Taka-10-shaku, hako-gata, tetsusuji-iri, jushinki nomi kashitsuke (Kyōto-shi kensetsu (kaku-tōrō-gata (dō-chō Shioda Chūzaeimon-shi kensetsu)_ 鹽
>
> 10 shaku [3.3m] tall, box-shaped, reinforced with steel, receiver donated by NHK. (Constructed by Kyoto City) square-lantern shaped (erected by Mr Chūzaieimon Shioda of Funaoka)

[Who was Shioda?]{.adm}

The park is built around Funaoka-yama, a raised area of land once the site of a 15th century castle, it now adjoins the grounds of Kenkun-jinja shrine (properly, Takeisao jinja) devoted to Oda Nobunaga and constructed during the Meiji period (1868-1912). The radio tower is located on a raised path on the north east slope of the hill around which the park is built, it overlooks a broad sandy plaza typical of parks in Japan. The placement was no doubt influenced by what was perceived at the time as being the primary functions of the radio towers, that is, to involve the local population in the daily morning 'rajio taisō' exercises. The plaza is a conveniently spacious area for a crowd of exercisers to gather. Behind the radio tower is a raised stage which was originally a bandstand [@koen40 5].

![Funaoka Park radio tower is positioned to overlook the sandy plaza below](https://i.imgur.com/NhZ63GD.png){width=60% #fig:funaoka}

This tower was restored in 2012 and a bronze plaque explaining its role and giving general information about radio towers has been mounted on its north side. Local people still gather to perform their morning exercise here, though a request by the group who took on the task of preserving the tower to re-install a working radio was rejected on the grounds that it might cause a disturbance to local residents.[^asahi] It is one of the few radio towers to still have an active user group; @ichima17[30-1] reports users gathering every morning before the 7am 'rajio taiso' broadcast to install a radio into the tower, which houses an electrical socket. [how many people now?]{.adm}

[^asahi]: http://www.asahi.com/kansai/travel/kansaiisan/OSK201205230020.html

## Komatsubara Park ## {.unnumbered}
Erected in 1940 in the south-east quadrant of this now rather dilapidated park, the front face of this tower is decorated with a plaque which reads;

> *Shinshin rensei* (心身練成)
> *kigen 2600-nen kinen kensetsu*

![This 2.7m tower, on its raised podium, dominates the ?? corner of this rather dilapidated park.](https://i.imgur.com/9EzZkGF.png){width=60% #fig:komatsu}

It is inscribed with the four-character idiomatic phrase _shin-shin-ren-sei_, roughly equivalent to _mens sana in corpore sano_ (a healthy mind in a healthy body) and obviously here acting as an exhortation to those using the park and perhaps especially people joining on with the early morning radio exercises.

## Hagi-jidō Park ## {.unnumbered}
<!-- Est'd: 1941 (kigen 2601) -->
Placed in the centre of this park in 1941, this 2.9 m tall tower has a design unlike other towers mentioned here, or indeed anywhere else in Japan. Whereas the majority of towers are symmetrical in plan,  this one seems to be aimed in one particular direction, towards the eastern half of the park. The front face still bears the letters JOOK, the call sign of the NHK Kyoto radio station. On the rear is a small plaque recording the names of those who contributed to the tower's construction and dating it to the year *kigen* 2601, or 1941.

![Unusual shaped, non-symmetric tower still retains JOOK call sign and plaque marking its setting up in 1940.](https://i.imgur.com/pb1OgY1.jpg){width=70% #fig:hagi-comp}

Standing a few metres to the north of the radio tower is a fairly nondescript concrete pillar roughly a metre in height, a wide semi-circular groove runs down its back and attached over the groove are three rusted metal fixings; given the co-occurrence of radio towers and flag-stands, it is probable that this was erected during the same period as the radio tower and was intended to support a flagpole.

## Tachibana Park ## {.unnumbered}
Standing 2.3m tall in the middle of the western edge of this park, this radio tower was renovated, along with the rest of the park, in 2013; both the outer and inner surfaces of the tower itself have been coated in concrete, A plaque on the east-facing side reads 'Tachibana Kōen Aigokai' (Tachibana Park Supporters Association). The portion of the tower that previously held the speakers has been treated more kindly and the south face still retains part of one of the metal grilles put in place to protect the internal workings.

![Tachibana Park, shows few of its original features after restoration](https://i.imgur.com/eOcBanO.png){width=60% #fig:tachibana}

The park itself was established on the site of a large restaurant (_ryōtei_) in July 1939 as part of a plan which aimed to expand open spaces for children by creating seven new parks [@koen40 6--7]. The creation of these 'children's parks' (_jidō kōen_) marked the birth of Crown Prince Akihito in 1933 [@doi91 174]. Another of the parks designated at this time was Kosaka Park, later also to contain a radio tower (no longer extant).

## Yase Park ## {.unnumbered}

Unlike most of the other towers described here, this one is located not in an urban park but at the foot of Mt. Hiei to the northest fo the city. There is no longer any obvious place for people to gather for their morning exercise session as the tower is positioned on a fairly steep slope close to  the Yase cable-car station. At the foot of this slope, adjacent to the Takana River that flows nearby, are a number of flatter areas - now apparently aimed at autumn-leaf viewers.

![Near the Mt.Hiei cable-car station, Yase valley](https://i.imgur.com/kewxTth.png){width=60% #fig:yase}

As can be seen in @fig:yase, the call sign for the Kyoto NHK station, JOOK, were prominently displayed on the 2.8m tower's front face. It is fairly typical in design with a rectangular cross-section and four openings in its uppermost section, but relatively plain and it has been perhaps 'renovated' at some point as there is no longer any visible access door or other exterior sign of the fact that it was once a public radio.

A nearby sign offers general details of radio towers but no specific information about this one, describing it only as a 'rare reminder of the daily lives of ordinary people long ago'.[^txln]

[^txln]: _ōji no shomin no seikatsu wo shinobaseru mezurashii isan_

## Murasakinoyanagi Park ## {.unnumbered}
This tower is situated at the eastern end of a fairly large park (apx 3000m^2^), it is, unlike the others mentioned here, built into a larger structure consisting of a concrete plaza, roughly 11x4m, and some bench seating. 2.4m tall. Date of construction unknown.

![Murasakinoyanagi Park radio tower and seating area.](https://i.imgur.com/7d9gScs.png){width=70% #fig:murasaki}

A semi-circular groove roughly 6cm across runs up the centre of the rear of this tower, this was to allow it to act as flag stand. The connection between the national flag, the state, and radio broadcasting is embodied in the design of this and similar installations, which were not uncommon [@ichima17 43].

This park [@doi91 173] opened in May 1935 as a designated children's park, part of a city-wide plan to increase the amount of outside space available for children to play in, the Takada Report of 1926 had raised concerns that the majority of Kyoto's children spent their time playing in the street or indoors (ibid.).

## Misayama Park ## {.unnumbered}
Moved from the grounds of the nearby Takakura Primary School (previously Nisshō) in 1995 and now standing 2.7m tall in the southeast quadrant of the park surrounded by the stone bench which seems to have been added at the time the tower was relocated. It is unusual in being round but this design seems to have been influenced by the name of the school in which it was originally situated, the Nisshō Primary School. The plan of the tower and bench seems to have been designed to resemble the 'mon' (crest) of the school which can be seen on the memorial in the grounds of Takakura School (@fig:mark). The lion heads are stylistically perhaps somewhat out of character, they were originally fountains with water accumulating in the space between the tower and bench [@ichima17 30].

![Misayama park radio tower, originally positioned in the grounds of the adjacent Takakura Primary School](https://i.imgur.com/c4QhSHy.png){width=60% #fig:misayama}

The Nisshō School, along with four others --- originally founded as '???' in the early years of the Meiji period --- was merged into the new Takakura School in 1993. [Was the tower moved then or before?]{.adm}

![Nisshō School memorial, incorporating the school's 'mon'crest (highlighted)](https://i.imgur.com/OVx1zZi.png){width=60% #fig:mark}

This radio tower appears in a children's picture book [@matsum08] by local artist, Matsumoto Yuka, in a story which brings together various wartime events which affected the residents in the area around the park. It includes a description of people gathering around the tower - then situated in the primary school grounds next to a gingko tree (still standing) - to listen to Emperor Hirohito announce Japan's surrender on 15 August 1945. According to Ms Matsumoto[^matsu] it was originally surrounded by a low, plain concrete wall surmounted by a handrail. This formed a small pond around the tower in which fish lived.

[^matsu]: Personal communication, 27 February 2019.

<!-- 2.7m tall. date of construction - unknown. -->

## Timeline ##

The 1932 NHK Radio Yearbook [@nhknenkan32] lists 20 NHK broadcast stations, including the JODK station in Seoul, or Keijō-fu as it was referred to during the period during which Korea was occupied by Imperial Japan. In passing it is interesting to note that the Seoul Station had a call sign which was obviously considered part of the domestic sequence prefixed with JO, whereas the stations established in Taipei and Dalian were given different prefixes, JF and JQ respectively.

In the three years before 1942 there was a huge expansion in the number of radio towers; the first facility had been set up in 1930 and in the nine years until 1939 the number of towers had gradually crept up to 60 [@nhknenkan40 277-8]. The next 12 month saw the creation of more than 200 more installations,  [@nhknenkan41 322-4] lists 269 radio towers. The next twelve months saw a similar pace of expansion and the following year's list mentions 412 sites.

The plaque on the Funaoka Tower (put there by Kyoto City) says that 'more than 300' towers were set up between 1930 and the end of WW2, however @ichima17 mentions that there were over 450, though not all of these have been confirmed.

Area      | 1940 | 1941 | 1942
:---------|-----:|-----:|----:
Tokyo     |   13 |   62 |   93
Osaka     |   17 |   61 |   80
Nagoya    |    4 |    4 |   31
Hiroshima |    6 |   43 |   74
Kumamoto  |    8 |   38 |   55
Sendai    |    4 |   32 |   43
Sapporo   |    8 |   29 |   36
TOTAL     |   60 |  269 |  412
: Increase in number of radio towers listed in NHK Yearbook, column headings indicate yearbook year. {#tbl:increase}

<!-- 1paxNGi33etwphkg-QMbudDNp9hOFDe-RXRu6z018WZU -->

After the publication of the 1942 yearbook, as Japan headed deeper into the Pacific War and wartime requisitioning began affect access to paper and raw materials - there was no 1944 yearbook!, the number of radio towers ultimately constructed seems to have been in the region of 450 [@mainichi20090722].

# Postwar mass media #

Prewar and wartime broadcasting was exclusively in the hand of NHK. commercial radio only emerged in ???.

Were newspapers involved in setting up commercial radio stations ? Did the Yomiuri get involved? When? How? Where?

Shoriki had taken over the Yomiuri Newspaper in January 1924, and while it was not one of the larger newspapers at the time, he would certainly have been aware of his competitors involvement in radio broadcasting, through their applications for the broadcasting licenses made available in Tokyo, Osaka and Nagoya after the issuance of formal regulations on 20 December 1923[^toshiba] [CEHCK DETAILS in HOSO-SHI NHK]{.adm}

[^toshiba]: http://toshiba-mirai-kagakukan.jp/learn/history/ichigoki/1924radio/index_j.htm & http://www.soumu.go.jp/main_sosiki/joho_tsusin/policyreports/joho_tsusin/houtai/pdf/080225_1_s6.pdf

Was this experience put to work during applications for television licences and in the decision to set up 'public televisions'?


# Discussion

Radio towers were falling into disuse even as the Pacific War entered its third year, requisitioning meant that many of the radios were removed.
Striking - differences in design
Centralisation of content, but high degree of local discretion in implementation.

Over the past decade there has been an increased awareness of the value of these and similar urban an industrial as heritage objects, and a number of local governments have made moves to register radio towers as 'cultural properties'[^cult-ex] thus ensuring they will be preserved.

[^cult-ex]: The radio tower in Maebashi Central Park (Maebashi City, Gunma) was registered as cultural property number 10-0247, in December 2007. That in Nakazaki Park in Akashi City was registered (No. 28-0556) in March 2013.

# References # {.unnumbered}
<div id="refs"></div>


!ifdef(pae)(# Images #)()

!ifdef(pae)(!rawinc(!root/images-at-end.md))
